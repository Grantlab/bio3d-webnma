FROM bio3d/bio3d-base:latest

MAINTAINER Lars Skjærven "larsss@gmail.com"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
    libcairo2-dev \
    libxt-dev \
    libx11-dev \
    libglu1-mesa-dev \
    xorg \
    xtail \
    texlive \
    texlive-latex-extra

WORKDIR /src

# Download and install phmmer
RUN wget --no-verbose http://eddylab.org/software/hmmer/hmmer-3.2.1.tar.gz && \
    tar xzvf hmmer-3.2.1.tar.gz && \
    cd hmmer-3.2.1 && \
    ./configure --prefix=/src/hmmer && make && make install && \
    mv /src/hmmer/bin/phmmer /usr/bin/phmmer && \
    cd /src && rm -rf hmmer* 

# Download and install shiny server 
RUN wget --no-verbose https://download3.rstudio.org/ubuntu-14.04/x86_64/VERSION -O "version.txt" && \
    VERSION=$(cat version.txt)  && \
    wget --no-verbose "https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-$VERSION-amd64.deb" -O ss-latest.deb && \
    gdebi -n ss-latest.deb && \
    rm -f version.txt ss-latest.deb && \
    . /etc/environment

# Setup R configs
RUN echo "r <- getOption('repos'); r['CRAN'] <- 'http://cran.us.r-project.org'; options(repos = r);" > ~/.Rprofile

# Install required R packages
RUN R -e "install.packages('devtools')"
RUN R -e "devtools::install_version('plyr')"
RUN R -e "devtools::install_version('maptools')"
RUN R -e "devtools::install_version('reshape2')"
RUN R -e "devtools::install_version('abind')"
RUN R -e "devtools::install_version('rjson')"
RUN R -e "devtools::install_version('pander')"
RUN R -e "devtools::install_version('data.table')"

# These will install various dependencies
RUN R -e "devtools::install_version('rmarkdown')"
RUN R -e "devtools::install_version('shiny')"
RUN R -e "devtools::install_version('shinyBS')"
RUN R -e "devtools::install_version('shinyjs')"
RUN R -e "devtools::install_version('threejs')"

# RGL: unable to open X11 display
RUN R -e "devtools::install_version('rglwidget')"
RUN R -e "devtools::install_github('ramnathv/rCharts')"
RUN R -e "devtools::install_github('rstudio/DT')"

# Bio3d
RUN R -e "devtools::install_bitbucket('Grantlab/bio3d', subdir = 'ver_devel/bio3d/')"
RUN R -e "devtools::install_bitbucket('Grantlab/bio3d-view')"

# fetch pdb_seqres.txt
RUN mkdir /net && mkdir /net/shiny_data && mkdir /net/seqres
RUN cd /src && wget ftp://ftp.wwpdb.org/pub/pdb/derived_data/pdb_seqres.txt.gz && \
     gunzip pdb_seqres.txt.gz && mv pdb_seqres.txt /net/seqres/pdb_seqres.txt

#COPY ./pca-app /srv/shiny-server/pca-app
COPY . /srv/shiny-server/nma-app

#COPY ./config.r.docker /srv/shiny-server/pca-app/config.r
COPY ./config.r.docker /srv/shiny-server/nma-app/config.r

COPY shiny-server.sh /usr/bin/shiny-server.sh
COPY shiny-server.conf /etc/shiny-server/shiny-server.conf

EXPOSE 3838

WORKDIR /srv/shiny-server/
CMD ["/usr/bin/shiny-server.sh"]
